#!/bin/bash

#remove old versions
sudo apt-get remove docker docker-engine docker.io

# install dependencies
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# add repository and install
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce

# post instalation
sudo groupadd docker
sudo usermod -aG docker $USER